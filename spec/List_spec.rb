require 'spec_helper'
require 'gem'

describe Node do
  before :each do
    @var1 = Bibliography.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide","The Facets of Ruby","Pragmatic Bookshelf",4,"July 7, 2013",["9781937785499","1937785491"])
    @var2 = Bibliography.new(["Scott Chacon"],"Pro Git 2009th Edition","Pro","Apress",2009,"August 27, 2009",["9781430218333","1430218339"])

    @node1 = Node.new(@var1, @var2)
    @node2 = Node.new(@var2, nil)

  end
  
  describe "Prueba de Nodes" do
    it "Se crea node 1" do
      expect(@node1.value.to_s).to eq("Dave Thomas, Andy Hunt, Chad Fowler\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide\nThe Facets of Ruby\nPragmatic Bookshelf; 4 edicion (July 7, 2013)\nISBN-13: 9781937785499\nISBN-10: 1937785491\n")
    end
    
    it "Se crea node 2" do
      expect(@node2.value.to_s).to eq("Scott Chacon\nPro Git 2009th Edition\nPro\nApress; 2009 edicion (August 27, 2009)\nISBN-13: 9781430218333\nISBN-10: 1430218339\n")
    end
    
    it "Se muestra el siguiente nodo" do
        expect(@node1.next.to_s).to eq("Scott Chacon\nPro Git 2009th Edition\nPro\nApress; 2009 edicion (August 27, 2009)\nISBN-13: 9781430218333\nISBN-10: 1430218339\n")
    end
  end  
end

describe LinkedList do
    before :each do
        @var1 = Bibliography.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide","The Facets of Ruby","Pragmatic Bookshelf",4,"July 7, 2013",["9781937785499","1937785491"])
        @var2 = Bibliography.new(["Scott Chacon"],"Pro Git 2009th Edition","Pro","Apress",2009,"August 27, 2009",["9781430218333","1430218339"])
        @var3 = Bibliography.new(["David Flanagan", "Yukihiro Matsumoto"],"The Ruby Programming Language","O'Reilly Media",1,"February 4, 2008",["0596516177","9780596516178"])
        @var4 = Bibliography.new(["David Chelimsky", "Dave Astels", "Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec,Cucumber, and Friends","The Facets of Ruby","Pragmatic Bookshelf",1,"December 25, 2010",["1934356379","9781934356371"])
        @var5 = Bibliography.new(["Richard E"], "Silverman Git Pocket Guide","O'Reilly Media",1,"(August 2, 2013)",["1449325866","9781449325862"])
    
        @node1 = Node.new(@var1, nil)
        @node2 = Node.new(@var2, nil)
        @node3 = Node.new(@var3, nil)
        @node4 = Node.new(@var4, nil)
        @node5 = Node.new(@var5, nil)
        @list1 = LinkedList.new([@node1,@node2,@node3])
        @list2 = LinkedList.new([@node2])
        @list3 = LinkedList.new([@node3])
        @list4 = LinkedList.new([@node4])
        
        
        
    end
    
    describe "Prueba con Pop" do
        it "Mostramos el 1º nodo con un pop" do
            expect(@list1.pop).to eq "Dave Thomas, Andy Hunt, Chad Fowler\nProgramming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide\nThe Facets of Ruby\nPragmatic Bookshelf; 4 edicion (July 7, 2013)\nISBN-13: 9781937785499\nISBN-10: 1937785491\n"
        end
    
        it "El siguiente pop deberia mostrar node 2" do
            @list1.pop
            expect(@list1.pop).to eq "Scott Chacon\nPro Git 2009th Edition\nPro\nApress; 2009 edicion (August 27, 2009)\nISBN-13: 9781430218333\nISBN-10: 1430218339\n"
        end
    end
    
    describe "Pruebas con push" do
        it "insertamos un elemento" do
            @list2.push(@node3)
            expect(@list2.start.next).to eq(@node3)
        end
    end
    
    describe "Pruebas con multiples elementos" do
        it "Insertamos varios elementos" do
            @list3.push_mej([@node5,@node4])
            @list3.pop
            expect(@list3.start).to eq(@node5)
            expect(@list3.start.next).to eq(@node4)
        end
    end
    
    describe "Debe existir una Lista con su cabeza" do
        it "La lista no puede estar vacia" do
            expect {@lista4.pop }.to raise_error
        end
    end
end


