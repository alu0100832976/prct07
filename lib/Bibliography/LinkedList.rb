Node = Struct.new(:value, :next)

class LinkedList
    attr_reader :start 
    def initialize(nodes)
        nodes.each do |i|
            raise unless i.is_a? Node
            #puts i
        end
        
        x = nodes.length
        x -= 1
        @start = nodes[0]
        if x > 1
            @start.next = nodes[1]
            for j in 1..x do
                nodes[j].next = nodes[j+1]
            end
        end
        
    end
    
    def pop
        raise if @start.next == nil
        aux = @start.value.to_s
        @start = @start.next
        aux
    end
    
    def push(node)
        raise unless node.is_a? Node
        pos = @start
        while pos.next != nil do
            pos = pos.next
        end
        pos.next = node
    end
    
    def push_mej(nodes)
        raise unless nodes.is_a? Array
        for x in nodes do
            raise unless x.is_a? Node
        end
        pos = @start
        while pos.next != nil do
            pos = pos.next
        end
        y = nodes.length
        y -= 1
        for z in 0..y do
            pos.next = nodes[z]
            pos = pos.next
        end
    end
end